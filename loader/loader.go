package configuration_loader

import (
	"github.com/spf13/viper"
	"gitlab.com/nariflix/libraries/utils/strings"
	"log"
	"reflect"
)

/*
Load reads the config like spring where the configurations are
setted in a yml called application and the simillar configurations
are overrided according the environment
*/
func Load(env *string) error {
	viper.AddConfigPath("resources")

	// Carrega o arquivo de configuração padrão
	viper.SetConfigName("application")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	// Carrega o arquivo de configuração específico do ambiente, se houver
	if env != nil {
		viper.SetConfigName("application-" + *env)
		if err := viper.MergeInConfig(); err != nil {
			log.Printf("Nenhum arquivo de configuração específico para o ambiente '%s' encontrado. Usando apenas configuração padrão.", *env)
		}
	}
	return nil
}

func GetValue(key string) string {
	return viper.GetString(key)
}

func GetSliceValue(key string) []string {
	return viper.GetStringSlice(key)
}

func GetStructValue(key string, object interface{}) {
	data := viper.GetStringMap(key)
	populateData(object, data)
}

func populateData(properties interface{}, data map[string]interface{}) {
	v := reflect.ValueOf(properties).Elem()
	for key, value := range data {
		if value == nil || value == "" {
			log.Println("Skipping property empty: ", key)
			continue
		}

		fieldName := strings.SnakeCaseToCamelCase(key)
		field := v.FieldByName(fieldName)
		if !field.IsValid() {
			log.Println("Skipping invalid field: ", fieldName)
			continue
		}

		if field.Kind() == reflect.Slice {
			sliceVal, ok := value.([]interface{})
			if !ok {
				log.Println("Skipping invalid slice field: ", fieldName)
				continue
			}
			strSlice := make([]string, len(sliceVal))
			for i, val := range sliceVal {
				strSlice[i] = val.(string)
			}
			field.Set(reflect.ValueOf(strSlice))
		} else if field.Kind() == reflect.String {
			field.SetString(value.(string))
		} else if field.Kind() == reflect.Bool {
			field.SetBool(value.(bool))
		} else if field.Kind() == reflect.Int {
			field.SetInt(int64(value.(int)))
		}
	}
}
